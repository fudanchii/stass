from __future__ import print_function
import json, jobs
from main import app

import flask as fl

@app.route('/enqueue', methods=['POST'])
def enqueue():
    params = json.loads(fl.request.data)
    if len(params['url']) == 0:
        fl.abort(403)
    jobs.run_funkload.delay(params)
    return 'Okay\n'

@app.route('/enqueue_test', methods=['POST', 'GET'])
def test():
    print(fl.request.data)
    return fl.request.data

@app.route('/')
def index():
    return fl.render_template('index.html')

