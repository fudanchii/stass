import os, sys, subprocess, shutil, time
import tarfile
from contextlib import closing
from datetime import datetime

import template

def run(params):
    dr = create_template(params['title'])
    loop_exec(dr, params)

def create_template(name):
    rootdir = '/tmp'
    src = os.path.join(rootdir, 'fltemplate')
    target = os.path.join(rootdir, name)
    if os.path.exists(src):
        shutil.rmtree(src)
    with closing(tarfile.open('fltemplate.tar.gz', 'r:gz')) as tar:
        tar.extractall(rootdir)
    if os.path.exists(target):
        shutil.rmtree(target)
    os.rename(src, target)
    return target

def loop_exec(dr, params):
    for url in params['url']:
        params['hostee'] = url
        template.gen(dr, params)
        run_stresstest(dr)
        time.sleep(2) # We're not in hurry

def run_stresstest(dirname):
    pp = subprocess.Popen(['make', 'bench'], cwd=dirname)
    pp.communicate()

    basename = os.path.basename(dirname)
    report1dir = os.path.join(dirname, 'report-onecpu')
    if os.path.isdir(report1dir):
        shutil.copytree(report1dir, os.path.join('/data/reports', basename + '_' + tstamp()))

def tstamp():
    return datetime.today().strftime('%Y%m%d%H%M%S')
