from flask.ext.rq import job
import time

@job
def puts_and_sleep(wat):
    time.sleep(5)
    print("hmmmm: %s\n" % wat)
    time.sleep(2)

@job
def run_funkload(params):
    import funkloadrunner as fl
    import mail
    result = fl.run(params)
    mail.send(result, params['email'])
