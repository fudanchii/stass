import flask
from flask.ext.rq import RQ

app = flask.Flask(__name__, \
      template_folder='../templates', static_folder='../static')

app.debug = True

rq = RQ(app)

import routes
import middleware
