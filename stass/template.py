import os, jinja2

templateLoader = jinja2.FileSystemLoader( searchpath="/" )
templateEnv = jinja2.Environment( loader=templateLoader )

templatenames = {
    'Simple.conf.j2': 'Simple.conf',
    'Makefile.j2': 'Makefile'
}

def render(tpl, params):
    tmpl = templateEnv.get_template(tpl)
    return tmpl.render(params)

def gen(dir, params):
    for k, v in templatenames.iteritems(): 
        outputfile = open(os.path.join(dir, v), 'w')
        outputfile.write(render(os.path.join(dir, k), params))
        outputfile.close()
